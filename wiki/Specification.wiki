#summary Linked Data API Specification
#labels Featured

This document defines a vocabulary and processing model for a configurable API layer intended to support the creation of simple RESTful APIs over RDF triple stores. 

The API layer is intended to be deployed as a proxy in front of a SPARQL endpoint to support:

  * Generation of documents (information resources) for the publishing of Linked Data
  * Provision of sophisticated querying and data extraction features, without the need for end-users to write SPARQL queries
  * Delivery of multiple output formats from these APIs, including a simple serialisation of RDF in JSON syntax

== Table of Contents == 

  # [http://code.google.com/p/linked-data-api/wiki/API_Rationale Rationale]
  # [http://code.google.com/p/linked-data-api/wiki/API_Deployment_Example Deployment Example]
  # [http://code.google.com/p/linked-data-api/wiki/API_Processing_Model Processing Model]
   # [http://code.google.com/p/linked-data-api/wiki/API_Identifying_An_Endpoint Identifying An Endpoint]
   # [http://code.google.com/p/linked-data-api/wiki/API_Binding_Variables Binding Variables]
   # [http://code.google.com/p/linked-data-api/wiki/API_Selecting_Resources Selecting Resources]
   # [http://code.google.com/p/linked-data-api/wiki/API_Viewing_Resources Viewing Resources]
   # [http://code.google.com/p/linked-data-api/wiki/API_Formatting_Graphs Formatting Graphs]
  # [http://code.google.com/p/linked-data-api/wiki/API_Property_Paths Property Paths]
  # [http://code.google.com/p/linked-data-api/wiki/API_Query_Parameters Query Parameters]
  # [http://code.google.com/p/linked-data-api/wiki/API_Vocabulary Vocabulary]

==Document Conventions==

The examples used in this document assume that the following namespaces have been defined, unless otherwise states

||rdf||`http://www.w3.org/1999/02/22-rdf-syntax-ns#`||
||rdfs||`http://www.w3.org/2000/01/rdf-schema#`||
||xsd||`http://www.w3.org/2001/XMLSchema#`||
||api||`http://purl.org/linked-data/api/vocab#`||

API configuration examples use the Turtle syntax for readability in the specification. This is not meant to constrain the syntaxes supported by implementations.