#summary Design issues - bNodes - graphs

== bNodes - graphs ==

bNodes referenced from more than one place need some form of ID (and then there's the same question of when to nest the JSON objects as above).

The question is what format should that ID take and how identifiable should it be?

RDFj uses `"<bnode:abc>"`. 

*Status:* open